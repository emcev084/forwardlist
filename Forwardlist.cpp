#include <iostream>
#include "Forwardlist.h"


Forwardlist::Forwardlist() { } //     

							   //     
void Forwardlist::insert(int data) {
	Node *n = new Node{ data, nullptr };
	if (tail) {
		tail->Next = n;
	}
	if (!head) {
		head = n;
	}
	tail = n;
}

//     
void Forwardlist::print() {
	if (this->isEmpty()) {
		std::cout << " Список пуст\n";
		return;
	}
	Node* cur = head;
	std::cout << cur->data;
	while (cur->Next != NULL) {
		cur = cur->Next;
		std::cout << "-> " << cur->data;
	}
	std::cout << std::endl;
}

void Forwardlist::remove(int val) {
	Node *n = head;
	if (n == nullptr) {
		std::cout << "Элемент " << val << " отсутствует\n";
		return;
	}
	if (n -> data == val) {
		Node *cur = head;
		head = head -> Next;
		delete cur;
		return;
	}
	else {
		while (n != nullptr) {
			if (n -> Next == nullptr) {
				std::cout << "Элемент " << val << " отсутствует\n";
				return;
			}
			if (n -> Next->data == val) {
				Node *cur = n -> Next;
				n -> Next = n -> Next -> Next;
				delete cur;
				return;
			}
			n = n -> Next;
		}
	}
}

bool Forwardlist::isEmpty() {
	return head == nullptr;
}

Node * Forwardlist::getHead() { 
	return head;
}

Forwardlist::~Forwardlist() {
	Node*cur = head;
	while (cur != nullptr) {
		Node*temp = cur -> Next;
		delete cur;
		cur = temp;
	}
} //.  

void Forwardlist::change(int position, int newval) {
	Node *n = head;
	int i = 0;
	while (n != nullptr) {
		if (i == position) {
			n ->data = newval;
			return;
		}
		i++;;
		n = n -> Next;
	}
	std::cout << "Элемент с позицией " << position << " не существует\n";
}

void Forwardlist::find(int val) {
	Node *n = head;
	int counter = 0;
	int result = 0;
	while (n != nullptr) {
		if (n -> data == val) {
			std::cout << "Элемент номер: " << counter << "\n";
			result++;
		}
		n = n -> Next;
		counter++;
	}
	if (result == 0) {
		std::cout << "Элемент не найден\n";
	}
}

void Forwardlist::sort()
{
	if (head == nullptr) {
		return;
	}
	Node* n1, *n2;
	for (n1 = head; n1 != nullptr; n1 = n1 -> Next) {
		for (n2 = n1; n2 != nullptr; n2 = n2 -> Next) {
			if (n1 -> data > n2 -> data)
			{
				int cur = n1 -> data;
				n1 -> data = n2 -> data;
				n2 -> data = cur;
			}
		}
	}
}
	