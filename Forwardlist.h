#pragma once

struct Node //     
{
	int data; //    
	Node *Next; // 
};


class Forwardlist // 
{
	Node *head = nullptr; //    
	Node *tail = nullptr; // 
public:
	Forwardlist(); //     
	~Forwardlist(); //.     
	void insert(int); //     
	void print(); // 
	void remove(int);
	void find(int);
	void change(int, int);
	void sort();

	Node* getHead();
	bool isEmpty();
};
