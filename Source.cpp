#include <iostream>
#include <cstdlib>
#include "Forwardlist.h"
#include <string>

using namespace std;

void ClearBuffer() {
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
}


int PrintMenu()
{
	int choice = 0;
	cout << "Выберите одну из операций :" << endl;
	cout << "1. Распечатать список" << endl;
	cout << "2. Добавить элементы в список" << endl;
	cout << "3. Удалить элемент" << endl;
	cout << "4. Найти позиции элементов" << endl;
	cout << "5. Заменить элемент на другой" << endl;
	cout << "6. Отсортировать элементы списка" << endl;
	cout << "7. Завершить работу программы" << endl;
	while (!(cin >> choice) || choice <= 0 || choice > 7)
	{
		ClearBuffer();
		cout << "Введите целое число от 1 до  7\n" << endl;
	}
	return choice;
}

int main(int argc, char* argv[])
{
	setlocale(LC_ALL, "rus");
	Forwardlist fl;
	if (argc == 2) {
		char * input = argv[1];
		char * pt = strtok_s(input, ",", &input);
		while (pt != nullptr) {
			fl.insert(atoi(pt));
			pt = strtok_s(nullptr, ",", &input);
		}
	}
	if (argc > 2) {
		for (int i = 1; i < argc; ++i) {
			fl.insert(atoi(argv[i]));
		}
	}

	do {
		switch (PrintMenu()) {
		case 1: {
			fl.print();
			break;
		}
		case 2: {
			cout << "Введите значение для вставки:\n";
			int data;
			cin >> data;
			fl.insert(data);
			break;
		}
		case 3: {
			cout << "Введите значение элемента: ";
			int x;
				cin >> x;
			fl.remove(x);
			break;
		}
		case 4: {
			cout << "Введите значение элемента: ";
			int x = 0;
			cin >> x;
			fl.find(x);

			break;
		}
		case 5: {
			int position,newval;
			cout << "Введите позицию:\n";
			cin >> position;
			cout << "Введите новое значение\n";
			cin >> newval;
			fl.change(position, newval);
			break;
		}
		case 6: {
			fl.sort();
			break;
		}
		case 7: {
			cout << "Вы действительно хотите выйти из программы? (y/n)\n ";
			string ans;
			while (true)
			{
				ClearBuffer();
				getline(cin, ans);
				ClearBuffer();
				if (ans == "y" || ans == "Y" || ans == "Yes" || ans == "yes" || ans == "YES")
				{
					cout << endl << "До свидания!\n";
					return 0;
				}
				else
				{
					if (ans == "n" || ans == "N" || ans == "No" || ans == "no" || ans == "NO")
						break;
					else
					{
						cout << "Введите допустимый ответ:\n ";
						continue;
					}

				}
			}
			break;
		}
		}
	} while (true);

	system("PAUSE");

}
